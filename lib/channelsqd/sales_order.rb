module Channelsqd
  class SalesOrder

    attr_reader :data
    def initialize(attributes={})
      @data = attributes[:data]
    end

    # WIP
    def create(data, order_id) 
      url = Channelsqd.channel_url(["order", order_id])
      HTTParty.put(url,
      { 
        :body => data.to_json,
        :headers => Channelsqd::Configuration.adaptor_headers
      })
    end

    # DIRECT creation to Channel SQUID API (dev)
    def self.create_so_to_squid(data, order_id)
      url = "https://fulfillment.api.acommercedev.com/channel/shopifydummyclient/order/#{order_id}"
      HTTParty.put(url,
      { 
        :body => data.to_json,
        :headers => Channelsqd::Configuration.headers
      })
    end

    def self.retrieve(order_id)
      url = Channelsqd.channel_url(["order", order_id])

      HTTParty.get(url, :headers => Channelsqd::Configuration.headers).body
    end

    def self.retrieve_status_update(iso_date, status) 
      # COMPLETED , NEW, IN_PROGRESS, CANCELLED
      date = Channelsqd.filterize_content((DateTime.now - 1).iso8601, iso_date)

      url = Channelsqd.channel_url("sales-order-status?since=#{date}&orderStatus=#{status}")
      HTTParty.get(url, :headers => Channelsqd::Configuration.headers).body
    end
  end
end
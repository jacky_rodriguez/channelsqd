module Channelsqd
  class ItemAllocation

    def self.inventory
      url = Channelsqd.channel_url(["allocation", "merchant", Channelsqd.configuration.merchant_id]) #("allocation/merchant/1175")
      HTTParty.get(url, :headers => Channelsqd::Configuration.headers).body
    end
  end
end
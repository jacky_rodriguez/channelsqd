module Channelsqd
  class Merchant

    # Old name authenticate_merchant
    def self.merchant_list
      url = Channelsqd.channel_url("merchants")
      HTTParty.get(url, :headers => Channelsqd::Configuration.headers).body
    end

    def self.adaptor_merchant_list
      url = Channelsqd.channel_url("merchants", false)
      datas = HTTParty.get(url, :headers => Channelsqd::Configuration.adaptor_headers).body
      JSON.parse(datas)["data"]
    end

  end
end

require "channelsqd/version"
require "channelsqd/configuration"
require "channelsqd/sales_order"
require "channelsqd/item_allocation"
require "channelsqd/merchant"
require "yaml"
require "httparty"

module Channelsqd
  module_function

  def load_config
      config_file = 'config/api_channelsqd_config.yml'
    if Dir.glob(config_file).any?
      YAML.load_file(config_file)
    else
      puts 'Initializing default configuration...'
    end
  end

  def configuration
    @configuration ||= Configuration.new(Channelsqd.load_config)
  end

  def filterize_content(default_content, content)
    # Use this to Avoid White space / Empty string
    content.to_s.strip.length == 0 ? default_content : content;
  end

  def channel_url(sub_link = "", api_connection=true)
    # Default -- connect to squid API Else Used SQUID-ADAPTOR 
    api_channel = api_connection ? configuration.channel_url : configuration.channel_url_adaptor
    File.join(api_channel, configuration.channel_id, filterize_content([], sub_link))
  end
end
